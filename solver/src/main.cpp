//============================================================================
// Name        : HelloWord.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include "pEnumeratorDouble.hpp"
#include "Configurator.hpp"
#include "Utils.hpp"

#include <omp.h>
#include <NTL/mat_RR.h>
#include <cmath>
#include <string.h>

using namespace std;
using namespace NTL;

int main(int argc, char** argv)
{
#pragma omp parallel
#pragma omp single nowait
	cout << "Working threads: " << omp_get_num_threads() << endl;

#ifdef USE_MB_TYPES
	cout << "Using MB types." << endl;
#else
	cout << "Using NTL types." << endl;
#endif

	if(readConfig("") < 0)
		if(readConfig("/home/mburger/Dokumente/p3enum/penum/") < 0)
			readConfig("/home/mburger/p3enum/penum/");
		

	std::string filepath=""; // Basisfile that is read
	std::string vecpath=""; // Vectorfile that is read

    int mode = 1;     	// 1 = Do basis reduction and search
                    	// 2 = Do LLL/BKZ and write base to file

	int candidate_height=2; // How long are the candidate vectors searched in serial


	// Parse input arguments
    for(int i = 1; i < argc; i++) {
    	std::string input = std::string(argv[i]);

        if(input == "--basisfile") {
            if(argc <= i) {
                std::cerr << "Missing basisfile in argument. Terminating." << std::endl;
                exit(-1);
            }

            filepath = std::string(argv[i+1]);
            i++;
        }

        if(input == "--prunefile") {
            if(argc <= i) {
                std::cerr << "Missing prunefile in argument. Terminating." << std::endl;
                exit(-1);
            }

            Configurator::getInstance().ext_pruning_function_file = std::string(argv[i+1]);
            i++;
        }

        if(input == "--vecfile") {
            if(argc <= i) {
                std::cerr << "Missing vectorfile in argument. Terminating." << std::endl;
                exit(-1);
            }

            cout << "Only executing vector length mode." << endl;
            vecpath = std::string(argv[i+1]);
            i++;
        }

        if(input == "--beta") {
            if(argc <= i) {
                std::cerr << "Missing beta in argument. Terminating." << std::endl;
                exit(-1);
            }

            int beta = atoi(argv[i+1]);
            Configurator::getInstance().glob_beta = beta;
            i++;
        }

        if(input == "--sheight") {
            if(argc <= i+1) {
                std::cerr << "Missing sheight in argument. Terminating." << std::endl;
                exit(-1);
            }

            candidate_height = atoi(argv[i+1]);
            Configurator::getInstance().forced_height = candidate_height;
            i++;
        }
        
        if(input == "--prebeta") {
             if(argc <= i+1) {
                std::cerr << "Missing prebeta in argument. Terminating." << std::endl;
                exit(-1);
            }   

            int prebeta = atoi(argv[i+1]);
            Configurator::getInstance().prebeta = prebeta;
            cout << "Setting prebeta parameter to " << prebeta << "." << endl;
            i++;  
        }       
        
        if(input == "--pruneparam") {
             if(argc <= i+1) {
                std::cerr << "Missing pruneparam in argument. Terminating." << std::endl;
                exit(-1);
            }   

            double prune_param = atof(argv[i+1]);
            Configurator::getInstance().prune_param = prune_param;
            cout << "Setting pruning parameter to " << prune_param << "." << endl;
            i++;  
        }

        if(input == "--enumpruning") {
            if(argc <= i+1) {
                std::cerr << "Missing enumpruning in argument. Terminating." << std::endl;
                exit(-1);
            }
            std::string min_prd(argv[i+1]);
            Configurator::getInstance().enum_prune = to_prune(min_prd);
            cout << "Activating " << min_prd << " pruning." << endl;
            i++;
        }

        if(input == "--nogaussestimate") {
        	Configurator::getInstance().do_gauss_estimate = false;
        }

        if(input == "--Enumthreads") {
            if(argc <= i+1) {
                std::cerr << "Missing Enumthreads in argument. Terminating." << std::endl;
                exit(-1);
            }

            Configurator::getInstance().Enumthreads = atoi(argv[i+1]);
            i++;
        }        
        
        if(input == "--candsize") {
            if(argc <= i+1) {
                std::cerr << "Missing candsize in argument. Terminating." << std::endl;
                exit(-1);
            }

            Configurator::getInstance().cand_queue_size = atoi(argv[i+1]);
            i++;
        }

        if(input == "--parthres") {
            if(argc <= i+1) {
                std::cerr << "Missing parthres in argument. Terminating." << std::endl;
                exit(-1);
            }

            Configurator::getInstance().par_threshold = atoi(argv[i+1]);
            i++;
        }

        if(input == "--delta") {
            if(argc <= i) {
                std::cerr << "Missing delta in argument. Terminating." << std::endl;
                exit(-1);
            }

            double delta = atof(argv[i+1]);
            Configurator::getInstance().glob_delta = delta;
            i++;
        }

        if(input == "--amax") {
            if(argc <= i) {
                std::cerr << "Missing amax in argument. Terminating." << std::endl;
                exit(-1);
            }

            double amax = atof(argv[i+1]);
            Configurator::getInstance().Amax = amax;
            i++;
        }


        if(input == "--aend") {
            if(argc <= i) {
                std::cerr << "Missing aend in argument. Terminating." << std::endl;
                exit(-1);
            }

            double aend = atof(argv[i+1]);
            Configurator::getInstance().Aend = aend;
            i++;
        }

        if(input == "--mbenum") {
            Configurator::getInstance().use_ntl_enum = false;
        }

        if(input == "--itenum") {
        	Configurator::getInstance().iterative_enumeration = true;
        }

        if(input == "--onlyLLLexport") {
            if(argc <= i) {
                std::cerr << "Missing output file argument for LLL. Terminating." << std::endl;
                exit(-1);
            }

            cout << "Base will be exported to: " << std::string(argv[i+1]) << endl;
            Configurator::getInstance().reducedfilepath = std::string(argv[i+1]);
            mode = 2;
            i++;
        }

        if(input == "--noLLL") {
        	Configurator::getInstance().dolll = false;
        }
    }

    std::cout << "Delta: " << Configurator::getInstance().glob_delta << std::endl;
    std::cout << "Beta: " << Configurator::getInstance().glob_beta << std::endl;
    std::cout << "Mode: " << mode << std::endl;
    std::cout << "Candidate Height: " << candidate_height << std::endl;
    std::cout << "Target length: " << Configurator::getInstance().Amax << std::endl;
    std::cout << "No. of threads in ENUM: " << Configurator::getInstance().Enumthreads << std::endl;

    // Read lattice basis from file
    if(filepath == "") {
    	std::cerr << "No valid filepath submitted. Exiting." << std::endl;
    	exit(-1);
    }

    // Execute if the parallel randomizing approach ist chosen


    // Standard procedure
    NTL::mat_ZZ B;
    std::ifstream basestream;
    std::cout << "Trying to read: " << filepath << std::endl;
    basestream.open(filepath.c_str(), ios_base::in);

    if(!basestream.is_open()) {
    	cerr << "Invalid basis at: " << filepath << ". Terminating." << endl;
    	exit(-1);
    }

    try {
    	basestream >> B;
    }
    catch (std::exception & e) {
    	cout << "Error while reading basis. Exiting" << std::endl;
    	exit(-1);
    }
    basestream.close();

    std::cout << "Read a basis of dimension: "
    		<< B.NumRows() << " / " << B.NumCols()
			<< " with |b_0| = " << lengthOfVector<ZZ, RR> (B[0])
			<< std::endl;



    cout << "Starting solving procedure" << endl;

	int dim = B.NumCols();

	pEnumeratorDouble pener = pEnumeratorDouble(dim);

	vec_ZZ vec; vec.SetLength(B.NumRows() + 3);

	pener.solveSVPSerial(B,vec);


   return 0;
}
