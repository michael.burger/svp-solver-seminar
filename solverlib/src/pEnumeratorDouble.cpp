/*
 * pEnumeratorDouble.cpp
 *
 *  Created on: 22.06.2018
 *      Author: mburger
 */


#include "pEnumeratorDouble.hpp"
#include "Configurator.hpp"
#include "Utils.hpp"
#include "MBVec.hpp"
#include "pruningfunc.hpp"

#include <iostream>
#include <iomanip>      // std::setprecision
#include <math.h>
#include <omp.h>
#include <limits>
#include <fstream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <vector>

// fpllltest
#include <cstdlib>
#include <fplll.h>


void printVector(double* v, int s, int e) {
	for(int i=s; i<=e; i++)
	cout	 << v[i] << " ";
	cout << endl;
}


pEnumeratorDouble::pEnumeratorDouble() {
	cout << "Constructor of pEnumeratorDouble called without dimension parameter!"
			<< endl;

	umin = NULL;
	v = NULL;
	l = NULL;
	c = NULL;

	// To decide the zigzag pattern
	Delta  = NULL;
	delta = NULL;

	_dim = -1;
	_cnt = 2;
	_cnt2 = 0;
    _cand_cnt = 0;
	_num_threads = 1;

	candidates_queue = NULL;
	tstart_bench = NULL;
	node_cnt = 0;

	r = NULL;
	sigma = NULL;
}

pEnumeratorDouble::pEnumeratorDouble(int dim) {
	_dim = dim;
	_num_threads = omp_get_max_threads();

	//candidates_queue = MBVecQueue2<double>(Configurator::getInstance().cand_queue_size);
	candidates_queue = new MBVecQueue3(Configurator::getInstance().cand_queue_size);// storage for the candidates for SVs

	prunfunc.SetLength(_dim+3);
	_conf = pruning_conf {NO, 1.05};

	umin = new double*[_num_threads + 3];
	v = new double*[_num_threads + 3];
	l = new double*[_num_threads + 3];
	c = new double*[_num_threads + 3];
	Delta = new double*[_num_threads + 3];
	delta = new double*[_num_threads + 3];

	for(int i=0; i<=_dim+1; i++) {
		prunfunc[i] = 0;
	}

	for(int i=0; i < _num_threads+2; i++) {
		umin[i] = new double[_dim + 3];
		v[i] = new double[_dim + 3];
		l[i] = new double[_dim + 3];
		c[i] = new double[_dim + 3];
		Delta[i] = new double[_dim + 3];
		delta[i]  = new double[_dim + 3];

		for(int j=0; j <_dim+2; j++) {
			umin[i][j] = 0.0;
			v[i][j] = 0.0;
			l[i][j] = 0.0;
			c[i][j] = 0.0;
			Delta[i][j] = 0.0;
			delta[i][j] = 0.0;
		}
	}

	// Values for center-caching within enumeration
	this->r = new int*[this->_num_threads];
	this->sigma = new double**[this->_num_threads];

	for (int nt = 0; nt < this->_num_threads; nt++) {
		this->r[nt] = new int[ _dim + 4];
		this->sigma[nt] = new double*[ _dim + 4 ];
		for(int i = 0; i < _dim + 4; i++) {
			this->sigma[nt][i] = new double[ _dim + 4 ];
		}
	}

	_cnt = 1;
	_cnt2 = 0;
    _cand_cnt = 0;
	tstart_bench = NULL;
	node_cnt = NULL;
}

pEnumeratorDouble::~pEnumeratorDouble() {
	if(_dim > 0)
	{
		for(int i=0; i < _num_threads+2; i++) {
			delete[] umin[i];
			delete[] v[i];
			delete[] l[i];
			delete[] c[i];
			delete[] Delta[i];
			delete[] delta[i];
		}

		delete[] umin;
		delete[] v;
		delete[] l;
		delete[] c;
		delete[] Delta;
		delete[] delta;
		delete candidates_queue;



		// Values for center-caching within enumeration
		if(this->r != NULL) {
			for(int nt=0; nt < this->_num_threads; nt++ ) {
				delete[] this->r[nt];
				for(int i = 0; i < this->_dim + 4; i++) {
					delete[] this->sigma[nt][i];
				}
				delete[] this->sigma[nt];
			}
			delete[] this->r;
			delete[] this->sigma;
		}
	}
}


double pEnumeratorDouble::solveSVPSerial(mat_ZZ& B, vec_ZZ& vec) {
		// Use external pruning functions
		if (Configurator::getInstance().ext_pruning_function_file.compare("NOT") != 0) {
			cout << "Reading pruning function from " << Configurator::getInstance().ext_pruning_function_file << endl;
			readPruning(Configurator::getInstance().ext_pruning_function_file);
		}

		vec_ZZ randint;
		randint.SetLength(5);
		randint[0]=-2; randint[1]=-1; randint[2]=0; randint[3]=1; randint[4]=2;

		double tstart_oall = omp_get_wtime();
		mat_ZZ Bcands; // Candidate for a good base to find the short vector there

		int numt = 1;
		int procnum = 0;

		int dimen = B.NumCols();
		double target_a = -1;
		double act_A = -1;

		// Choose the boundary for the shortest vector
		double theAval = Configurator::getInstance().Amax;
        act_A = theAval * theAval;
        target_a = act_A;

        // Update the pruning function with this target length
    	_conf = pruning_conf {Configurator::getInstance().enum_prune,
    		Configurator::getInstance().prune_param};
    	updatePruningFuncLoc(prunfunc.data(), _conf, act_A, dimen, 0, dimen-1);

        omp_set_num_threads(numt); // Use pre-defined threads for all consecutive parallel regions
        int do_searching = 1;
        int act_trial = 0;

        int tid = omp_get_thread_num();
  		mat_RR mu;
		vec_RR bstar;

		double** p_mu = new double*[B.NumRows()];
		for(int i=0; i<B.NumRows(); i++)
			p_mu[i] = new double[B.NumRows()];

		double* p_bstar = new double[B.NumRows()];
		double* p_u = new double[B.NumRows()+2];
		double* p_prunfunc = new double[B.NumRows()+1];

        while(do_searching > 0)
	    {
        	act_trial++;
			// Reset random basis
			Bcands = B;

			int seed = ((act_trial+1)*numt + (tid+1)*time(NULL));

			// Replace the copy of the original matrix by a randomization of it
			Bcands = randomizeMatrix(Bcands, randint, Bcands.NumCols(),seed);


			// Do BKZ
			int prebeta=Configurator::getInstance().prebeta;
			for(int bval=Configurator::getInstance().glob_beta; bval<=Configurator::getInstance().glob_beta; bval++) {
				double tstartbkz = omp_get_wtime();
				double tbkzexact_start = 1;
				double tbkzexact_end = 0;
				double tbkzheu_start = 1;
				double tbkzheu_end = 0;

				// Standard calculation in fpllls lib
				if(Configurator::getInstance().use_fplll_bkz)
				{
					// RAM based transformation from NTL to fplll matrix format
					ZZ_mat<mpz_t> m_tored;
					ZZ_mat<mpz_t> U;

					m_tored.set_cols(B.NumCols());
					m_tored.set_rows(B.NumRows());

					for(int r=0; r < B.NumRows(); r++) {
						for(int c=0; c < B.NumCols(); c++) {
							mpz_t aa;
							mpz_init(aa);

							std::stringstream ssa;
							ssa << Bcands[r][c];
							mpz_set_str( aa, ssa.str().c_str(),10);

							m_tored[r][c] = aa;
						}
					}

					vector<Strategy> strategies;


					// First run of pure BKZ with small beta
					tbkzexact_start = omp_get_wtime();
					if(Configurator::getInstance().prebeta > 0)
					{
						BKZParam param20(prebeta, strategies);
						if(prebeta > 20) {
							param20.flags |=  BKZ_AUTO_ABORT;
						}
						param20.delta = Configurator::getInstance().glob_delta;
						bkz_reduction 	(&m_tored, &U,  param20, FT_DEFAULT, 0);
					}
					tbkzexact_end = omp_get_wtime();

					// Second run with BKZ 2.0
					tbkzheu_start = omp_get_wtime();
					if(Configurator::getInstance().glob_beta > 0)
					{
						strategies = load_strategies_json(strategy_full_path("default.json"));
						BKZParam paramval(bval, strategies);
						paramval.delta = Configurator::getInstance().glob_delta;
						paramval.flags |= BKZ_AUTO_ABORT;
						bkz_reduction 	(&m_tored, &U,  paramval, FT_DEFAULT, 0);
					}
					tbkzheu_end = omp_get_wtime();

					for(int r=0; r < B.NumRows(); r++) {
						for(int c=0; c < B.NumCols(); c++) {
							std::stringstream ssb;
							ssb << m_tored[r][c];
							Bcands[r][c] = conv<ZZ>(ssb.str().c_str());
						}
					}
				}
				double tendbkz = omp_get_wtime();


				std::cout << "[Thread: " << tid << "]\t"
						<< "|b_0| of BKZ-" << prebeta << "/" << bval  << " reduced basis "
						<< lengthOfVector<ZZ, RR> (Bcands[0]) // length of vector in first row
						<< "(" << tendbkz-tstartbkz << "s "
						<< "[" << tbkzexact_end - tbkzexact_start << " s / "
						<<  tbkzheu_end - tbkzheu_start << "s ]"
						<< ")."
						<< std::endl;
			}

			// Now take randomized bases and enumerate
			procnum++;
			// Calculate Gram-Schmidt
			GSO(Bcands, mu, bstar);
			//double tstart1 = omp_get_wtime();

			for(int i=0; i < B.NumCols(); i++) {
				p_u[i] = 0;
				p_bstar[i] = 0;
				p_prunfunc[i] = 0;
			}

			for(int i = 0; i < B.NumRows(); i++) {
				p_bstar[i] = NTL::conv<double>(bstar[i]);
				for(int j = 0; j < B.NumCols(); j++) {
					p_mu[i][j] = NTL::conv<double>(mu[i][j]);
				}
			}
			int dim = B.NumCols();

			// Perform the actual enumeration on the randomized basis
			act_A = EnumDoubleSerial(p_mu, p_bstar, p_u, 0, dim-1, dim, act_A, 0);

			NTL::Mat<long int> Bd; Bd.SetDims(B.NumRows(), B.NumCols());

			// Copy back from fplll to NTL format
			for(int i = 0; i < B.NumRows(); i++) {
				for(int j = 0; j < B.NumCols(); j++) {
					Bd[i][j] = NTL::conv<long int>(Bcands[i][j]);
				}
			}

			// Standard behavior of Extreme Pruning
			// End if a vector smaller than target found
			if(act_A < target_a) {
				double tend_oall = omp_get_wtime();
				cout << "Finishing after " << act_trial+1 << " attemps in "
						<< tend_oall - tstart_oall << " s (processed "
						<< procnum << " bases)."
						<< endl;

				NTL::Vec<double> sol;	sol.SetLength(B.NumCols());
				for(int ii=0; ii<B.NumCols(); ii++)
					sol[ii] = p_u[ii];

				calcVectorLengthDouble<long int, double>(sol, Bd);
				do_searching = 0;
			}

			if(act_trial >= Configurator::getInstance().trials && do_searching > 0)
			{
				do_searching = -1;
			    double tend_oall = omp_get_wtime();
			    	cout << "Unsuccessful after " << Configurator::getInstance().trials << " attemps in "
			    			<< tend_oall - tstart_oall << " s (processed "
							<< procnum << " bases)."
			    			<< endl;
			}
	    }
		delete[] p_bstar;
		delete[] p_u;
		delete[] p_prunfunc;
		for(int i=0; i<B.NumRows(); i++)
		   delete[] p_mu[i];
		delete[] p_mu;

		return act_A;
}


double pEnumeratorDouble::EnumDoubleSerial(double** mu, double* bstar, double* u, int jj, int kk, int dim,
		double A, int vec_offset, bool is_bkz_enum) {

	cout << setprecision(5);

	//cout << "Executing ENUM with target length " << sqrt(A) << endl;

	///////
	double ret =  SerialEnumerationDouble(mu, bstar, u, prunfunc, jj, kk, dim+vec_offset, A);
	return ret;
}




double pEnumeratorDouble::SerialEnumerationDouble(double** mu, double* bstarnorm,
		double* u, MBVec<double> prunefunc_in, int j, int k, int dim, double Ain=-1) {

	int tid = omp_get_thread_num();
	long long nodecnt = 0;

	for(int i=0; i< _dim + 2; i++) {
		r[tid][i] = i;
		for(int j=0; j < _dim + 2; j++) {
			sigma[tid][i][j] = 0.0;
		}
	}

	double* umin = new double[dim + 2];
	double* v = new double[dim + 2];
	double* l = new double[dim + 2];
	double* c = new double[dim + 2];

	// To decide the zigzag pattern
	double* Delta = new double[dim + 2];
	double* delta = new double[dim + 2];

	int s;
	int t; // s d for zigzagpattern

	double A = bstarnorm[j] * 1.0001;
	if(Ain == -1)
		A = bstarnorm[j] * 1.0001;
	else
		A = Ain;

	for(long i=0; i <= dim + 1; i++) {
		c[i] = l[i] = 0.0;
		Delta[i] = v[i] = 0.0;
		delta[i] = 1.0;
		umin[i] = u[i] = 0.0;
	}

	s = t = j;

	for(long i=j; i <= k+1; i++) {
		umin[i] = u[i] = 0.0;
	}
	u[j] = umin[j] = 1;


	double tstart = omp_get_wtime();
	while (true) {
		l[t] = l[ t + 1 ] + ( (u[t]) + c[t] ) * ( (u[t]) + c[t] ) * bstarnorm[t];

		if(l[t] < prunefunc_in[t]) {
			nodecnt++;
			if(t > j) {
				t = t - 1;

				r[tid][t] = std::max<int>(r[tid][t], r[tid][t+1]);

				for(int j = r[tid][t+1]; j > t + 1; j--) {
					sigma[tid][t][j-1] = sigma[tid][t][j] + u[j-1] * mu[j-1][t];
				}
				r[tid][t+1] = t + 1;

				c[t] = sigma[tid][t][t+1];

				// Activate to not use caching
				//c[t] = muProdDouble(u, mu, t, s/*dim-1*/);
				u[t] = v[t] = (ceil(-c[t] - 0.5));

				// For ZigZag
				Delta[t] = 0;
				if(u[t] > -c[t]) {
					delta[t] = - 1;
				}
				else {
					delta[t] = 1;
				}
			}

			else {
				//updatePruningFuncLoc(prunefunc_in.data(), _conf, l[t], dim, j, k);
				A = l[t];

				for(long i=j; i <= k; i++) {
					umin[i] = u[i];
				}

				// Avoid visiting short vector twice
				// When bound is not updated
				t = t + 1;

				if(t > k)
					break;

				r[tid][t] = t+1;

				s = max(s,t);

				if (t<s)
					Delta[t] = -Delta[t];

				if(Delta[t] * delta[t] >= 0) Delta[t] += delta[t];
					u[t] = v[t] + Delta[t];
			}
		}

		else {
			t = t + 1;


			if(t > k)
				break;

			r[tid][t] = t+1;

			s = max(s,t);

			if (t<s)
				Delta[t] = -Delta[t];

			if(Delta[t] * delta[t] >= 0) Delta[t] += delta[t];
			u[t] = v[t] + Delta[t];
		}

	}

	for(long i=j; i <= k; i++) {
		u[i] = umin[i];
	}
	double tend = omp_get_wtime();

	cout << "[Thread: " << tid << "]\t"
		<< "Time ENUM: " << (tend - tstart)
			<< " / Speed:  " << (double)(nodecnt) / (tend - tstart) << " nodes/s ("
			<< nodecnt << " nodes)." << endl;

	// clean memory
	delete[] umin;
	delete[] v;
	delete[] l;
	delete[] c;
	delete[] Delta;
	delete[] delta;

	return A;
}




inline double pEnumeratorDouble::muProdDouble (double* x, double** mu, const int t, const int s) {
	double res(0);
	for(int i = t + 1; i <= s; i++) {
		res += x[i] * mu[i][t];
	}
	return res;
}

