/*
 * Enumerator.hpp
 *
 *  Created on: 22.06.2018
 *      Author: mburger
 */

#ifndef SRC_PENUMERATORDOUBLE_HPP_
#define SRC_PENUMERATORDOUBLE_HPP_

#include <NTL/LLL.h>

#include "MBQueue.hpp"
#include "MBVec.hpp"
#include "pruningfunc.hpp"

using namespace std;
using namespace NTL;
using namespace MB;

class pEnumeratorDouble {
public:
	pEnumeratorDouble();
	pEnumeratorDouble(int dim);

	~pEnumeratorDouble();

	pEnumeratorDouble( const pEnumeratorDouble& other ) {
		cout << "pEnumeratorDouble( const pEnumeratorDouble& other )" << endl;
	}
	pEnumeratorDouble( pEnumeratorDouble& other ) {
		cout << "pEnumeratorDouble( const pEnumeratorDouble& other )" << endl;
	}

	double solveSVPSerial(mat_ZZ& B, vec_ZZ& vec);

	double EnumDoubleSerial(double** mu, double* bstar, double* u, int jj, int kk, int dim, double A, int vec_offset=1, bool is_bkz_enum=false);

	long long get_cnt () {
		return _cnt;
	}
private:
	double SerialEnumerationDouble(double** mu, double* bstarnorm,
			double* u, MBVec<double> prunefunc_in, int j, int k, int dim, double Ain);

	inline double muProdDouble (double* x, double** mu, const int t, const int s);

	// Membervariables for the remainder search
	// General enumeration variables
	double** umin;
	double** v;
	double** l;
	double** c;


	// To decide the zigzag pattern
	double** Delta;
	double** delta;

	// To execute the benchmark
	unsigned long long* node_cnt;
	double * tstart_bench;

	// To reduce number of multiplications
	int** r;
	double*** sigma;

	MBVecQueue3* candidates_queue;
	//std::vector<MBVec<double> > candidates_vec;

	// General membervars
	int _dim;
	long long _cnt;
	long long _cnt2;
    long long _cand_cnt;
	int _num_threads;

	MBVec<double> prunfunc; // Array with pruning function
	pruning_conf _conf;
};


#endif /* SRC_PENUMERATORDOUBLE_HPP_ */
