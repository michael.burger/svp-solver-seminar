MAKE=make
BIN=solver

CXX = g++ 

SRCDIR=src
CPP_SRCS += \
src/main.cpp 

O_SRCS += \
src/main.o 

OBJS += \
src/main.o 

CPP_DEPS += \
src/main.d 


LANG=-std=c++0x
CXXFLAGS= -O3 -g -Wall -c -fmessage-length=0 -fopenmp -MMD -MP
DEFINES= -DUSE_VECQUEUE
LDFLAGS=-fopenmp -g
INCLUDE=-I../solverlib/src -I../ntl/include
LIBS=-lsolverlib -lgmp -lfplll -lm
LIBPATH=-L../solverlib -L../ntl/Release

src/%.o: src/%.cpp
	@echo 'Building file: $<'
	$(CXX) $(LANG) $(DEFINES) $(INCLUDE) $(CXXFLAGS) -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
 
all:
	$(MAKE) --no-print-directory main-build

# Main-build Target
main-build: $(BIN)
	cd solverlib && make
	cd solver && make


clean:
	cd solverlib && make clean
	cd solver && make clean 

echo:

	echo $(OBJS)

-include $(DEPS)

.PHONY: all clean


